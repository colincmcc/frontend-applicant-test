'use strict';

angular.
  module('countryList').
  component('countryList', {
    templateUrl: 'dist/country-list/country-list.template.html',
    controller: ['$http', function CountryListController($http) {
      var self = this;

      self.orderProp = 'countryName';
      self.countryProp = '';

      $http.get('data.json').then(
       response => {

         self.countries = Object.entries(response.data).map(([countryName, data]) => ({countryName, data}))
        console.log(self.countries)

         self.totals = self.countries.reduce((a, b) => {
           return {
            data: {
                  Devices: a.data.Devices + b.data.Devices,
                  Channels: a.data.Channels + b.data.Channels
                }
             }
          })
       }
      )
    }]
  })

