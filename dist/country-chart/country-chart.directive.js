'use strict'

angular.
  module('countryChart').
  directive('countryChart', function() {

    return {
      scope: {
        devices: '@devices',
        channels: '@channels'
      },
      link: function(scope, element, attrs) {
        var devices = parseInt(scope.devices)
        var channels = parseInt(scope.channels)

        // Add space for text - 25px
        var devWidth = devices + 25
        var chanWidth = channels +25

        // I chose to use canvas because it's been a recent interest of mine
        // Also, canvas allows for future development of more complex graph types as a future feature
        var canvas = element.parent()
        var ctx = canvas[0].getContext("2d")

        ctx.textAlign="center"
        ctx.textBaseline = "middle"
        canvas[0].width = devWidth + chanWidth

        // Device rectangle
        ctx.beginPath()
        ctx.rect(0, 0, devWidth,50)
        ctx.fillStyle = "#E75825"
        ctx.fill()
        ctx.fillStyle = "#fff"
        ctx.font = "20px Roboto"
        ctx.fillText(devices, (devWidth - 12.5)/2, 30 )

        // Channel rectangle
        ctx.beginPath()
        ctx.rect(devWidth, 0, chanWidth,50)
        ctx.fillStyle = "#fff"
        ctx.fillText(channels, devWidth + (chanWidth - 12.5)/2, 30 )
      }
    }
  })
